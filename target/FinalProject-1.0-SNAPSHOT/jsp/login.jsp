<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>


<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
</head>
<body>
<div class="wrapper">
<div class="login">
<form method="post" action="${pageContext.request.contextPath}/controller/login">

    <c:choose>
        <c:when test="${sessionScope.userNotFound != null}">
            ${sessionScope.userNotFound}
            <c:remove var="userNotFound" scope="session"/><br/>
        </c:when>
    </c:choose>
   <fmt:message key="label.enterLogin"/>
    <input type="text" name="login"><br/><br/>

    <c:if test="${sessionScope.incorrectPasswordError != null}">
        ${sessionScope.incorrectPasswordError}
        <c:remove var="incorrectPasswordError" scope="session"/><br/>
    </c:if>
    <fmt:message key="label.enterPassword"/>
    <input type="password" name="password"><br/><br/>

    <input class="btn btn-primary find" type="submit" value='<fmt:message key="label.logInTranslation"/>'/>

</form>
</div>
<h:change-language path="login"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>

</div>

</body>
</html>
