<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="f" uri="http://library.varenyk_mykyta.ntudp.dnipro.ua/functions" %>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>welcome_page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
<div class="wrapper">

<div class="search-sort">

<%@include file="/jsp/search_form.jsp"%>

<form class="sort" action="${pageContext.request.contextPath}/controller/sort"
method="get" onchange="submit()">
    <fmt:message key="label.sortBy"/><br/>
    <fmt:message key="label.nameTranslation"/>
    <input type="radio" name="sort_by" value="name">

    <fmt:message key="label.authorTranslation"/>
    <input type="radio" name="sort_by" value="author">

    <fmt:message key="index_jsp.publisher"/>
    <input type="radio" name="sort_by" value="publisher">

    <fmt:message key="label.dateOfIssue"/>
    <input type="radio" name="sort_by" value="date-of-issue">
</form>
</div>

<table id="books">
    <tr>
        <td class="content">
            <c:choose>
                <c:when test="${fn:length(sessionScope.booksList) == 0}">No such books</c:when>
                <c:otherwise>
                    <table id="search_results_table">
                        <thead>
                        <tr>
                            <td>№</td>
                            <td><fmt:message key="label.name"/></td>
                            <td><fmt:message key="label.author"/></td>
                            <td><fmt:message key="label.dateOfIssue"/></td>
                            <td><fmt:message key="label.count"/>/<fmt:message key="index_jsp.notAvailable"/></td>
                            <td><fmt:message key="index_jsp.publisher"/></td>
                        </tr>
                        </thead>
                        <c:forEach var="book" items="${sessionScope.booksList}">
                            <tr>
                                <td>${book.id}</td>
                                <td>${book.name}</td>
                                <td><c:forEach var="authorVar" items="${book.author}">
                                        ${authorVar}
                                    </c:forEach>
                                </td>
                                <td>${book.dateOfIssue}</td>
                                <c:set var="count" value="${f:countOrderedBooks(sessionScope.allOrders,book.id)}"/>
                                <td>
                                    <c:choose>
                                    <c:when test="${book.count >= count}">
                                        <jsp:text>${book.count - count}</jsp:text>
                                    <c:if test="${sessionScope.user.roleId == 2}">
                                <td>
                                    <div class="col s03">
                                        <form action="${pageContext.request.contextPath}/controller/user/take-home" method="post">
                                            <input type="number" hidden name="id" value="${book.id}">
                                            <input type="submit" class="button" value="take book home">
                                        </form>
                                    </div>
                                    <div class="col so3">
                                        <form action="${pageContext.request.contextPath}/controller/user/in-reading-hall"
                                              method="post">
                                            <input type="number" hidden name="id" value="${book.id}">
                                            <input type="submit" class="button" value="read book in reading hall">
                                        </form>
                                    </div>
                                </td>
                                </c:if>
                                </c:when>
                                <c:otherwise>
                                    <jsp:text>not available at the moment</jsp:text>
                                </c:otherwise>
                                </c:choose>
                                </td>
                                <td>${book.publisher}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>
<a href="/FinalProject/register.jsp" class="linkIndex">
    <fmt:message key="label.registerTranslation"/>
</a>

<a href="/FinalProject/jsp/login.jsp" class="linkIndex">
    <fmt:message key="label.logInTranslation"/>
</a>

<h:change-language path="redirect:/"/>
    <%@include file="/WEB-INF/jspf/footer.jspf"%>
</div>

</body>

</html>
