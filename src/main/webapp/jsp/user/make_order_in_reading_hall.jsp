<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:text>Make order: </jsp:text>
<div class="col s4">
    <div class="show-create-form">
        <form action="${pageContext.request.contextPath}/controller/user/confirm-in-reading-hall-order" method="post">
            <input type="number" hidden name="id" value="${requestScope.id}">

            <label for="hours_count">hours amount: </label>
            <input id="hours_count" type="number" min="1" max="4" required
                   placeholder="1-4" name="hours_count">
            <br>
            <input type="submit" class="btn" value="confirm"/>
        </form>
    </div>
</div>
</div>
</body>
</html>
