package ua.dnipro.ntudp.varenyk_mykyta.library.db.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Data access object for User entity.
 */

@Repository
public class UserDao{
    private static Logger LOGGER = LogManager.getLogger(UserDao.class);

    public static final String SQL_SELECT_ALL_USERS = "SELECT u.id, u.password, u.login, u.email, u.create_time, u.role_id, u.first_name, u.first_name_ua, u.last_name, u.last_name_ua, u.is_blocked, r.name as role, u.has_orders" +
            " FROM user u, role r" +
            " WHERE r.id=u.role_id ORDER BY u.id;";

    public static final String SQL_SELECT_USER_BY_LOGIN = "SELECT  u.id, u.login, u.email, u.has_orders, u.password, u.role_id, r.name as role, u.first_name, u.last_name, u.is_blocked " +
            "FROM user u, role r " +
            "WHERE u.login=? AND u.role_id=r.id";

    public static final String SQL_INSERT_NEW_USER = "INSERT INTO user (login, email, password, role_id, first_name, last_name)  " +
            "VALUES (?,?,?,?,?,?)";

    public static final String SQL_BLOCK_USER = "UPDATE user SET is_blocked=1 WHERE id=?";

    public static final String SQL_UNBLOCK_USER = "UPDATE user SET is_blocked=0 WHERE id=?";

    public static final String SQL_DELETE_LIBRARIAN = "DELETE FROM user WHERE id=?";

    public static final String SQL_INSERT_LIBRARIAN = "INSERT INTO user (login, email, password, first_name, last_name, role_id) VALUES (?,?,?,?,?,3)";

    public static final String SQL_SELECT_USERS_WITH_PAGINATION = "SELECT u.id, u.password, u.login, u.email, u.create_time, u.role_id, u.first_name, u.first_name_ua, u.last_name, u.last_name_ua, u.is_blocked, r.name as role, u.has_orders" +
            " FROM user u, role r" +
            " WHERE r.id=u.role_id LIMIT ?,?;";

    public static final String SQL_SELECT_NUMBER_OF_USERS = "SELECT COUNT(id) FROM user;";

    public static final String SQL_CHECK_IF_EMAIL_IS_PRESENT = "SELECT COUNT(1) FROM user WHERE email=?;";

    public static final String SQL_CHECK_IF_LOGIN_IS_PRESENT = "SELECT COUNT(1) FROM user WHERE login=?;";

    private DataSource dataSource;

    @Autowired
    public UserDao(DataSource dataSource){
        this.dataSource = dataSource;
    }

    /**
     * Returns all users.
     *
     * @return List of users.
     */
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        try(Connection con = dataSource.getConnection();
            Statement stmt = con.createStatement();
            ResultSet resSet = stmt.executeQuery(SQL_SELECT_ALL_USERS) ) {
            UserMapper userMapper = new UserDao.UserMapper();
            while (resSet.next()){
                users.add(userMapper.mapRow(resSet));
            }
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#getAll() -> {}",throwables);
            throwables.printStackTrace();
        }
        return users;
    }


    /**
     * Returns the number of users.
     *
     * Used in pair with UserDao#getUsers(int, int) to implement pagination.
     *
     * @see UserDao#getUsers(int, int)
     *
     * @return Amount of users integer value.
     */
    public int getNumberOfUsers(){
        int count = 0;
        try(Connection con = dataSource.getConnection();
        Statement statement = con.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_SELECT_NUMBER_OF_USERS)){
            if(resultSet.next()){
                count=resultSet.getInt(1);
            }
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#getNumberOfUsers() -> {}",throwables);
            throwables.printStackTrace();
        }
        return count;
    }

    /**
     * Returns users with specified limit and offset.
     *
     * Used in pair with UserDao#getNumberOfUsers() to implement pagination.
     *
     * @param currentPage limit
     *
     * @param recordsPerPage offset
     *
     * @see UserDao#getNumberOfUsers()
     *
     * @return List of users in specified limit and offset.
     */
    public List<User> getUsers(int currentPage, int recordsPerPage){
        List<User> users = new LinkedList<>();

        int start = currentPage * recordsPerPage - recordsPerPage;
        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_SELECT_USERS_WITH_PAGINATION)){
            statement.setInt(1,start);
            statement.setInt(2,recordsPerPage);

            try(ResultSet resultSet = statement.executeQuery()){
                UserMapper mapper = new UserMapper();
                while (resultSet.next()){
                    users.add(mapper.mapRow(resultSet));
                }
            }
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#getUsers(int,int) -> {}",throwables);
            throwables.printStackTrace();
        }
        return users;
    }


    /**
     * Returns user with specified login.
     *
     * @param login login of a searched user
     *
     * @return Matching user.
     */
    public User findUserByLogin(String login){
        User user = new User();

        try(Connection con = dataSource.getConnection();
            PreparedStatement statement = con.prepareStatement(SQL_SELECT_USER_BY_LOGIN)) {
            statement.setString(1,login);

            try(ResultSet resSet = statement.executeQuery()){
                if (resSet.next()){
                   UserMapper userMapper = new UserMapper();
                   user = userMapper.mapRow(resSet);
                   user.setLogin(login);
                }
            }
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#findUserByLogin(String) -> {}",throwables);
            throwables.printStackTrace();
        }
        return user;
    }

    /**
     * Blocks user with a specified id.
     *
     * @param id id of a user
     *
     * @return {@code true} if operation was successful,
     *{@code false} otherwise
     */
    public boolean blockUser(int id){
        boolean result = false;

        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_BLOCK_USER)){
            statement.setInt(1,id);
            result = statement.executeUpdate() == 1;
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#blockUser(int) -> {}",throwables);
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Unblocks user with a specified id.
     *
     * @param id id of a user
     *
     * @return {@code true} if operation was successful,
     *{@code false} otherwise
     */
    public boolean unblockUser(int id){
        boolean result = false;

        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_UNBLOCK_USER)){
            statement.setInt(1,id);
            result = statement.executeUpdate() == 1;
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#unblockUser(int) -> {}",throwables);
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Checks if user with specified email is already present in DB.
     *
     * @param email searched email
     *
     * @return {@code true} if a user with given email has been found ,
     *{@code false} otherwise
     */
    public boolean emailIsPresent(String email){

        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_CHECK_IF_EMAIL_IS_PRESENT)){
            statement.setString(1,email);

            try(ResultSet resultSet = statement.executeQuery()){
                if (resultSet.next() && resultSet.getInt(1) > 0){
                    return true;
                }
            }
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#emailIsPresent(String) -> {}",throwables);
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Checks if user with specified login is already present in DB.
     *
     * @param login searched login
     *
     * @return {@code true} if a user with given login has been found ,
     *{@code false} otherwise
     */
    public boolean loginIsPresent(String login){

        try(Connection con = dataSource.getConnection();
            PreparedStatement statement = con.prepareStatement(SQL_CHECK_IF_LOGIN_IS_PRESENT)){
            statement.setString(1,login);

            try(ResultSet resultSet = statement.executeQuery()){
                if (resultSet.next() && resultSet.getInt(1) > 0){
                    return true;
                }
            }
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#loginIsPresent(String) -> {}",throwables);
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Deletes a user, which has a user role "librarian".
     *
     * @param id id of a user
     *
     * @return {@code true} if operation was successful,
     *{@code false} otherwise
     */
    public boolean deleteLibrarian(int id){
        boolean result = false;

        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_DELETE_LIBRARIAN)){
            statement.setInt(1,id);
            result = statement.executeUpdate() == 1;
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#deleteLibrarian(int) -> {}",throwables);
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Inserts a user into DB with a role "librarian".
     *
     * @param user a user to be inserted
     *
     * @return {@code true} if operation was successful,
     *{@code false} otherwise
     */
    public boolean createLibrarian(User user){
        boolean result = false;
        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_INSERT_LIBRARIAN)){
            statement.setString(1,user.getLogin());
            statement.setString(2,user.getEmail());
            statement.setString(3,user.getPassword());
            statement.setString(4,user.getFirstName());
            statement.setString(5,user.getLastName());
            result = statement.executeUpdate() == 1;
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#createLibrarian(User) -> {}",throwables);
            throwables.printStackTrace();
        }
        return result;
    }

    public boolean createUser(User user) {
        boolean result = false;

        try(Connection con = dataSource.getConnection();
            PreparedStatement statement = con.prepareStatement(SQL_INSERT_NEW_USER)){
            statement.setString(1,user.getLogin());

            statement.setString(2,user.getEmail());

            statement.setString(3,user.getPassword());

            statement.setInt(4,user.getRoleId());

            statement.setString(5,user.getFirstName());

            statement.setString(6,user.getLastName());

            result = statement.executeUpdate() == 1;
        } catch (SQLException throwables) {
            LOGGER.error("SQL exception in UserDao#create(User) -> {}",throwables);
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Extracts user from the result set row.
     */
    private static class UserMapper implements EntityMapper<User> {

        @Override
        public User mapRow(ResultSet rs) {
            User user = new User();
            try {
                user.setId(rs.getInt(Fields.ENTITY_ID));
                user.setLogin(rs.getString(Fields.USER_LOGIN));
                user.setEmail(rs.getString(Fields.USER_EMAIL));
                user.setRoleId(rs.getInt(Fields.USER_ROLE_ID));
                user.setFirstName(rs.getString(Fields.USER_FIRST_NAME));
                user.setLastName(rs.getString(Fields.USER_LAST_NAME));
                user.setBlocked(rs.getInt(Fields.USER_STATUS));
                user.setRole(rs.getString(Fields.USER_ROLE));
                user.setPassword(rs.getString(Fields.USER_PASSWORD));
                user.setHasOrders(rs.getInt(Fields.USER_HAS_ORDERS));
            } catch (SQLException throwables) {
                LOGGER.error("SQL exception in UserMapper#mapRow(ResultSet) -> {}",throwables);
                throwables.printStackTrace();
            }
            return user;
        }
    }
}
