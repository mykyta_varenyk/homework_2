package ua.dnipro.ntudp.varenyk_mykyta.library.model;

/**
 * Role entity.
 *
 * @author Mykyta Varenyk
 *
 */

public enum Role {
    ADMIN, USER, LIBRARIAN;

    public static Role getRole(User user){
        int roleId = user.getRoleId();
        return Role.values()[roleId - 1];
    }

    public String getName() {
        return name().toLowerCase();
    }
}
