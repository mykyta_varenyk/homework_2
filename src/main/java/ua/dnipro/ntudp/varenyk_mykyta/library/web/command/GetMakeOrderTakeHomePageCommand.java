package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Returns a page with take home order form for a user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetMakeOrderTakeHomePageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetMakeOrderTakeHomePageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetMakeOrderTakeHomePageCommand started");

        int bookId = Integer.parseInt(req.getParameter(Fields.ENTITY_ID));

        req.setAttribute("id",bookId);

        LOGGER.debug("book id -> {}",bookId);

        LOGGER.debug("GetMakeOrderTakeHomePageCommand finished");

        return Path.PAGE_USER_MAKE_ORDER_TAKE_HOME_PAGE;
    }
}
