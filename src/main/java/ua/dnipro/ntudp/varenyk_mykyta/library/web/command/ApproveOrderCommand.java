package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Approve an order.
 *
 * @author Mykyta Varenyk
 */

@Service
public class ApproveOrderCommand extends Command {

    private OrderDao orderDao;

    private static Logger LOG = LogManager.getLogger(ApproveOrderCommand.class);

    @Autowired
    public ApproveOrderCommand(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOG.debug("ApproveOrderCommand started");

        HttpSession session = req.getSession();

        User librarian = (User) session.getAttribute("user");

        int librarianId = librarian.getId();

        LOG.debug("id of a librarian, that approves order -> {}",librarianId);

        int id = Integer.parseInt(req.getParameter(Fields.ENTITY_ID));

        LOG.debug("order id -> {}",id);

        try {
            orderDao = new OrderDao(DBManager.getDataSource());
        } catch (NamingException e) {
            LOG.error("NamingException in ApproveOrderCommand -> {}",e);
            e.printStackTrace();
        }

        boolean result = orderDao.approveOrder(librarianId,id);

        LOG.debug("order approved -> {}",result);

        LOG.debug("ApproveOrderCommand finished");

        return Path.PAGE_LIBRARIAN_LIST_USER_ORDERS;
    }
}
