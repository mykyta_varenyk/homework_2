package ua.dnipro.ntudp.varenyk_mykyta.library.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * User entity.
 *
 * @author Mykyta Varenyk
 *
 */

public class User extends Entity {
    private String login;

    private String email;

    private String password;

    private int roleId;

    private String firstName;

    private String lastName;

    private String firstNameUa;

    private String lastNameUa;

    private String role;

    private boolean isBlocked;

    private boolean hasOrders;

    public String getFirstNameUa() {
        return firstNameUa;
    }

    public void setFirstNameUa(String firstNameUa) {
        this.firstNameUa = firstNameUa;
    }

    public String getLastNameUa() {
        return lastNameUa;
    }

    public void setLastNameUa(String lastNameUa) {
        this.lastNameUa = lastNameUa;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public boolean getHasOrders() {
        return hasOrders;
    }

    public void setHasOrders(int hasOrders) {
        if (hasOrders == 1){
            this.hasOrders = true;
        }else {
            this.hasOrders = false;
        }
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(int blocked) {
        if (blocked == 1)
            isBlocked = true;
        else
            isBlocked = false;

    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roleId=" + roleId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstNameUa='" + firstNameUa + '\'' +
                ", lastNameUa='" + lastNameUa + '\'' +
                ", role='" + role + '\'' +
                ", isBlocked=" + isBlocked +
                ", hasOrders=" + hasOrders +
                '}';
    }
}
