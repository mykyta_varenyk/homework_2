package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Return an edit book page of admin user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetEditBookPageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetEditBookPageCommand.class);

    private BookDao bookDao;

    @Autowired
    public GetEditBookPageCommand(BookDao bookDao){
        this.bookDao = bookDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetEditBookPageCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        int id;

        if (req.getParameter("id") == null){
            id = Integer.parseInt(String.valueOf(session.getAttribute("bookId")));
        }else {
            id = Integer.parseInt(req.getParameter("id"));
        }

        LOGGER.debug("book id -> {}",id);

        Book book = bookDao.getBookById(id,languageCode);

        LOGGER.debug("book to be edited -> {}",book);

        req.setAttribute("book",book);

        List<Author> authors = bookDao.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}",authors);

        req.setAttribute("authors",authors);

        req.setAttribute("id",id);

        LOGGER.debug("GetEditBookPageCommand finished");

        return Path.PAGE_ADMIN_EDIT_A_BOOK_PAGE;
    }
}
