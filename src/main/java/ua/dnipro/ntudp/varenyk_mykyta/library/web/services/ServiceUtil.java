package ua.dnipro.ntudp.varenyk_mykyta.library.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;

@Component
public class ServiceUtil {
    private OrderDao orderDao;

    @Autowired
    public ServiceUtil(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    public static int getNumberOfPages(int rows, int recordsPerPage){
        return (int) Math.ceil(rows * 1.0/recordsPerPage);
    }
}
